public class Multiplier {
    int val1 = 0;
    int val2 = 0;
    public Multiplier (int x, int y)
    {
        val1 = x;
        val2 = y;
    }

    public int multiplied () {
        return val1 * val2;
    }
}

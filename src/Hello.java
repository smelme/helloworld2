import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.WebDriver;
import org.junit.*;
import java.io.IOException;
public class Hello {

    public static int sum (int x, int y) {
        return x+y;
    }
    public static void main(String[] args) {
        System.out.println("Console is: " + System.console());
        //System.out.println("Console is: test");
        System.out.println(sum(2,3));

        Multiplier m = new Multiplier(2,3);
        System.out.print(m.multiplied());
        System.setProperty("webdriver.chrome.driver", "D:/chromdriver/chromedriver.exe");
        WebDriver driver =new ChromeDriver();
        String test = System.getProperty("webdriver.chrome.driver");
        System.out.println("test is " + test);
        //FirefoxDriver driver=new FirefoxDriver();

        /*driver.get("http://demo.guru99.com/");
        WebElement element=driver.findElement(By.xpath("//input[@name='emailid']"));
        element.sendKeys("abc@gmail.com");

        WebElement button=driver.findElement(By.xpath("//input[@name='btnLogin']"));
        button.click();*/

       /* driver.get("https://www.bbc.com/");
        WebElement element=driver.findElement(By.xpath("//input[@id='orb-search-q']"));
        element.sendKeys("ethiopia");

        WebElement button=driver.findElement(By.xpath("//button[@id='orb-search-button']"));
        button.click();

        WebElement link=driver.findElement(By.xpath("//a[@class='css-rjlb9k-PromoLink ett16tt7']"));
        link.click();*/
        Action action = new Action ();
        try {
            action.timeOut(driver,30);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        action.getPage(driver, "https://www.bbc.com/");
        WebElement element = action.findElementXpath(driver,"//input[@id='orb-search-q']");
        action.setText(element,"ethiopia");
        WebElement button=action.findElementXpath(driver,"//button[@id='orb-search-button']");
        action.click(button);
        WebElement link=action.findElementXpath(driver,"//a[@class='css-rjlb9k-PromoLink ett16tt7']");
        action.click(link);
        action.closeBrowser(driver);
        action.closeSession(driver);

    }
}

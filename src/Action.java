import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.concurrent.TimeUnit;
import java.io.IOException;
public class Action {
    /*public Action (int x, int y)
    {

    }*/
    public void getPage (WebDriver driver, String url) {
        driver.get(url);
    }
    public void closeBrowser (WebDriver driver) {
        driver.close();
    }
    public void closeSession (WebDriver driver) {
        driver.quit();
    }
    public String getTitle (WebDriver driver) {
        return driver.getTitle();
    }
    public WebElement findElementXpath (WebDriver driver,String xpath) {
        WebElement element=driver.findElement(By.xpath(xpath));
           return element;
    }
    public WebElement findElementId (WebDriver driver,String id) {
        WebElement element=driver.findElement(By.id(id));
        return element;
    }
    public WebElement findElementName (WebDriver driver,String name) {
        WebElement element=driver.findElement(By.name(name));
        return element;
    }
    public WebElement findElementLink (WebDriver driver,String linkTxt) {
        WebElement element=driver.findElement(By.linkText(linkTxt));
        return element;
    }
    public WebElement findElementClass (WebDriver driver,String className) {
        WebElement element=driver.findElement(By.className(className));
        return element;
    }
    public WebElement findElementCSS (WebDriver driver,String cssSelector) {
        WebElement element=driver.findElement(By.cssSelector(cssSelector));
        return element;
    }
    public void click (WebElement object) {
        object.click();
    }
    public void setText (WebElement object,String text) {
        object.sendKeys(text);
    }
    public void timeOut (WebDriver driver,int time) throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(time,TimeUnit.SECONDS) ;
        //WebDriverWait wait=new WebDriverWait(driver, time);
    }
}
